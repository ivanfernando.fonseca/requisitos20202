package com.usta.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {

	public static boolean isAlphabetic(String valor) {
		for(int i=0;i<valor.length();i++){ 
			if(!Character.isLetter(valor.charAt(i)))
			   return false;
		}
		return true;
	}
		
	public static boolean isEmail(String email) {
		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	public static boolean min(int valor, int min) {
		if (valor<min)
			return true;
		return false;
	}
	
	public static boolean max(int valor, int max) {
		if (valor>=max)
			return true;
		return false;
	}
	
}
