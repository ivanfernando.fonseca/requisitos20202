package com.usta.logica;


public class Persona {
	private String nombres;
	private String  apellidos;
	private String  correo;
	private int telefono;
  
	enum cargo{
        JUNIOR, SENIOR, ARQUITECTO, PROJECT_MANAGER
    }

	
	public Persona(){}
	
	
	public Persona(String nombres, String apellidos, String correo, int telefono) {
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.correo = correo;
		this.telefono = telefono;
	}


	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	
	
	

    
    
   
}
